var modalForm = '.modal-form';
function modalMail(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        url: 'modal',
        type: 'POST',
        dataType: 'json',
        data: $(modalForm).serialize(),
        success: function(data){
            if(data.status){
                $.notify("Se envio el mensaje correctamente.", "success"), ({
                    type: "success",
                    delay: 10000
                });
                $(modalForm).trigger("reset");
                $('.modal').each(function(){
                    $(this).modal('hide');
                });
            }else{
                $.notify("Error al enviar al mensaje, por favor vuelva a intentar.", "error"),({
                    type: "error",
                    delay: 10000
                });
            }
        }
    });  
}

$(modalForm).on( "submit", function( event ) {
    event.preventDefault();
    modalMail();
    // console.log( $( this ).serialize() );
});
var errorNull = true, errorMail = true, errorPhone = true;
var checkNull = function(){
  $(this).val($(this).val().trim());
  if ($(this).val() =="") {
    $(this).notify("Se requiere llenar este campo", "error");
    $(this).addClass("errtextbox");
    errorNull = true;
  } else {
    errorNull = false;
    $(this).removeClass("errtextbox");
  }
};

$("#name").focusout(checkNull);
//$("#subject").focusout(checkNull);
//$("#message").focusout(checkNull);

$("#phone").focusout(function(){
  var value = $(this).val();
  if (value.length <= 9) { 
    $(this).notify("El número requiere hasta 10 dígitos. No incluya espacios ni otros caracteres.", "error");
    $(this).addClass("errtextbox");
    errorPhone = true;
  } else {
    if (value.length > 10) {
      $(this).notify("No más de 10 dígitos. No incluya espacios ni otros caracteres.", "error");
      $(this).addClass("errtextbox");
      errorPhone = true;
    } else {
      errorPhone = false;
      $(this).removeClass("errtextbox");
    }
  }
});

$("#email").focusout(function(){
  var value = $(this).val().trim();
  if (value.search(/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/) != 0) {
    $(this).notify("E-mail requiere un formato nombre@correo.com", "error");
    $(this).addClass("errtextbox");
    errorMail = true;
  } else { 
    $(this).removeClass("errtextbox");
    errorMail = false;
  }
});

$("#message").focusout(function(){
  var value = $(this).val();
  if (value.length <= 9) {
    $(this).notify("Escribe al menos 10 caracteres", "error");
    $(this).addClass("errtextbox");
    errorNull = true;
  } else {
    errorNull = false;
    $(this).removeClass("errtextbox");
  }
});
