<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('contacto', 'IndexController@contacto')->name('index.contacto');

Route::post('modal', 'IndexController@modal')->name('index.modal');

Route::get('/', function () {
    return view('inicio');
})->name('inicio');

Route::get('/lotes/lotes-130', function () {
    return view('lotes-130');
})->name('130');

Route::get('/lotes/lotes-165', function () {
    return view('lotes-165');
})->name('165');

Route::get('/lotes/lotes-185', function () {
    return view('lotes-185');
})->name('185');

Route::get('/lotes/lotes-condominales', function () {
    return view('lotes-condominales');
})->name('condominal');

Route::get('/plaza-comercial', function () {
    return view('plaza-comercial');
})->name('plaza');

Route::get('/zona-tulum', function () {
    return view('zona-tulum');
})->name('zona');

Route::get('/villas-accra', function () {
    return view('villas-accra');
})->name('villas');