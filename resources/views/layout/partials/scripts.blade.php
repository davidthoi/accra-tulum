<!--====== jquery js ======-->
    <!--====== jquery js ======-->
    <script src="{{ asset('assets/js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <!--====== Bootstrap js ======-->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
    <!--====== Slick js ======-->
    <script src="{{ asset('assets/js/slick.min.js') }}"></script>
    <!--====== Isotope js ======-->
    <script src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
    <!--====== Magnific Popup js ======-->
    <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
    <!--====== inview js ======-->
    <script src="{{ asset('assets/js/jquery.inview.min.js') }}"></script>
    <!--====== counterup js ======-->
    <script src="{{ asset('assets/js/jquery.countTo.js') }}"></script>
    <!--====== Nice Select ======-->
    <script src="{{ asset('assets/js/jquery.nice-select.min.js') }}"></script>
    <!--====== Bootstrap datepicker ======-->
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <!--====== Wow JS ======-->
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <!--====== Main js ======-->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-5HZM7W22R0"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'G-5HZM7W22R0');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '458950945392372');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=458950945392372&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->