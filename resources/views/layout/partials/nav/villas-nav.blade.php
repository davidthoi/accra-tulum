<!-- Main Menu -->
<div class="nav-menu d-xl-flex align-items-center">
    <!-- Mneu Items -->
    <div class="menu-items">
        @include('layout.partials.nav.menu-items')
    </div>
    <!-- from pushed-item -->
    <div class="nav-pushed-item"></div>
</div>

<!-- Site Logo -->
<div class="site-logo">
    <a href="<?php echo url('/') ?>" class="main-logo">
        <img src="{{ asset ('assets/img/villas/logo-villas-full.svg') }}" alt="Logo" style="width:62px;">
    </a>
    <a href="<?php echo url('/') ?>" class="sticky-logo">
        <img src="{{ asset ('assets/img/villas/logo-villas.svg') }}" alt="Logo" style="width:94px;">
    </a>
</div>

<!-- Header Info Pussed To Menu Wrap -->
<div class="nav-push-item">
<!-- Header Info -->
    <div class="header-info d-xl-flex align-items-center padding-telmail">
        @include('layout.partials.nav.contact-info')
    </div>
</div>

<!-- Navbar Toggler -->
<div class="navbar-toggler">
    <span></span><span></span><span></span>
</div>