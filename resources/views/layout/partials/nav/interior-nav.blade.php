<!-- Main Menu -->
<div class="nav-menu d-lg-flex align-items-center">

<!-- Menu Items -->
<div class="menu-items">
    @include('layout.partials.nav.menu-items')
</div>

<!-- from pushed-item -->
<div class="nav-pushed-item"></div>
</div>

<!-- Site Logo -->
<div class="site-logo">
    <a href="<?php echo url('/') ?>"><img src="{{ asset ('assets/img/inicio/accra_logo2.svg') }}" alt="Logo"></a>
</div>

<!-- Header Info Pussed To Menu Wrap -->
<div class="nav-push-item">
<!-- Header Info -->
    <div class="header-info d-lg-flex align-items-center">
    @include('layout.partials.nav.contact-info')
    </div>
</div>

<!-- Navbar Toggler -->
<div class="navbar-toggler">
<span></span><span></span><span></span>
</div>