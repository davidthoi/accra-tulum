<ul class="montserrat-sans">
    <li>
        <a class="{{ Request::is('/') ? 'active-accra' : '' }}" href="{{route('inicio')}}" class="d-xl-none">Inicio</a>
    <li>
        <a class=" {{ Request::is('lotes/lotes-130','lotes/lotes-165','lotes/lotes-185','lotes/lotes-condominales') ? 'active-accra' : '' }}" href="#">Lotes</a>
        <ul class="submenu">
            <li><a class="{{ Request::is('lotes/lotes-130') ? 'active-accra-drop' : '' }}" href="{{route('130')}}">Lotes Residenciales 130 m<sup>2</sup></a></li>
            <li><a class="{{ Request::is('lotes/lotes-165') ? 'active-accra-drop' : '' }}" href="{{route('165')}}">Lotes Residenciales 165 m<sup>2</sup></a></li>
            <li><a class="{{ Request::is('lotes/lotes-185') ? 'active-accra-drop' : '' }}" href="{{route('185')}}">Lotes Residenciales 185 m<sup>2</sup></a></li>
            <li><a class="{{ Request::is('lotes/lotes-condominales') ? 'active-accra-drop' : '' }}" href="{{route('condominal')}}">Lotes Condominales</a></li>
        </ul>
    </li>
    <li>
        <a class="{{ Request::is('zona-tulum') ? 'active-accra' : '' }}" href="{{route('zona')}}">Zona Tulum</a>
    </li>
    <li>
        <a href="<?php echo url('/') ?>#amenities">Amenidades</a>
    </li>
    <li>
        <a class="{{ Request::is('plaza-comercial') ? 'active-accra' : '' }}" href="{{route('plaza')}}">Comercial</a>
    </li>
    <li>
        <a class="{{ Request::is('villas-accra') ? 'active-accra' : '' }}" href="{{route('villas')}}">Villas</a>
    </li>
    <li>
        <a href="<?php echo url('/') ?>#contacto">Contacto</a>
    </li>
</ul>