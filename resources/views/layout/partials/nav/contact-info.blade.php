<div class="item">
    <i class="iconos-sticky-menu fal fa-phone"></i>
    <span>Teléfono de contacto</span>
    <a href="tel:+529842762391" target="_blank" rel="noopener noreferrer nofollow">
        <h5 class="title">+52 (984) 276 23 91</h5>
    </a>
</div>
<div class="item">
    <i class="iconos-sticky-menu fal fa-envelope"></i>
    <span>Email</span>
    <a href="mailto:sales@lotusrivieramaya.com" target="_blank" rel="noopener noreferrer nofollow">
        <h5 class="title">sales@lotusrivieramaya.com</h5>
    </a>
</div>