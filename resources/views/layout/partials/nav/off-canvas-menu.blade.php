<!-- Navbar Close Icon -->
<div class="navbar-close">
    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
</div>

<!-- Off canvas Menu  -->
<div class="toggle">
    <a href="#" id="offCanvasBtn"><i class="fal fa-bars"></i></a>
</div>