<!--====== FOOTER PART START ======-->
	<footer>
		<div class="pt-120 pb-120" style="background-image:url({{ asset ('assets/img/inicio/back_footeraccra.jpg') }}">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="subscribe-text text-center">
							<div class="footer-logo mb-45">
								<img src="{{ asset ('assets/img/inicio/accra-logo3.svg') }}" alt="images">
							</div>
							<p class="txt-under">Avenida Coba Norte s/n, Coba Norte, 77780 Tulum, Q.R.</p>
							<p class="txt-under"><a href="tel:+529842762391" target="_blank" rel="noopener noreferrer nofollow">+52 (984) 276 23 91</a></p>
							<p class="txt-under"><a href="mailto:sales@lotusrivieramaya.com" target="_blank">sales@lotusrivieramaya.com</a></p>
							<div class="copyright-area pt-20 pb-20" style="background-color:transparent;">
								<div class="container">
									<div class="row align-items-center">
										<div class="col-md-7">
											<div class="social-links">
												<a href="https://www.facebook.com/Accra-Tulum-Residencial-112146047306288/" target="_blank"><i class="fab fa-facebook-f"></i></a>
												<a href="https://www.instagram.com/accratulum/" target="_blank"><i class="fab fa-instagram"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="fb-root"></div>
      	<script>
      	  window.fbAsyncInit = function() {

      	    FB.init({

      	      xfbml            : true,

      	      version          : 'v10.0'

      	    });

      	  };
      	  (function(d, s, id) {

      	    var js, fjs = d.getElementsByTagName(s)[0];

      	    if (d.getElementById(id)) return;

      	    js = d.createElement(s); js.id = id;

      	    js.src = 'https://connect.facebook.net/es_LA/sdk/xfbml.customerchat.js';

      	    fjs.parentNode.insertBefore(js, fjs);

      	  }(document, 'script', 'facebook-jssdk'));

      	</script>
      	<!-- Your plugin de chat code -->
      	<div class="fb-customerchat"

        attribution="page_inbox"

        page_id="112146047306288">
      	</div>
		<div class="copyright-area pt-20 pb-20">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-5">
						<p class="copyright-text">ACCRA TODOS LOS DERECHOS RESERVADOS</p>
					</div>
					<div class="col-md-5">
						<p class="copyright-text">DEVELOPED BY
                        <a href="https://thoi.art" style="color: #072651; text-decoration: none;">THOI</a>
                        </p>
					</div>
				</div>
			</div>
		</div>
	</footer>
    <!--====== FOOTER PART END ======-->