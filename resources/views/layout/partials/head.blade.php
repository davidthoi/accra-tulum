<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--====== Title ======-->
    <title> Accra | @yield('title') </title>
    <!--====== Favicon Icon ======-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::asset('assets/img/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::asset('assets/img/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('assets/img/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/img/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('assets/img/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/img/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('assets/img/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/img/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('assets/img/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ URL::asset('assets/img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ URL::asset('assets/img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('assets/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ URL::asset('assets/img/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ URL::asset('assets/img/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#072651">
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.min.css') }}">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}" />
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}" />
    <!--====== Flaticon css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/flaticon.css') }}" />
    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/magnific-popup.css') }}" />
    <!--====== Owl Carousel css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/slick.css') }}" />
    <!--====== Nice Select ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/nice-select.css') }}" />
    <!--====== Bootstrap Datepicker ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-datepicker.css') }}" />
    <!--====== Default css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/default.css') }}" />
    <!--====== Style css ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}" />
    <!--====== Accra icons ======-->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/accra.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/custom.css') }}" />
    @stack('styles')
    <!--====== Tags ======-->
    <meta name="facebook-domain-verification" content="c51e4y7iub6d9tmxcynf0o25cfwq0w" />
    <!-- Global site tag (gtag.js) - Google Ads: 679548598 --> 
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-679548598"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-679548598'); </script>
    <!-- Event snippet for Registro - Accra Tulum conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
    <script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-679548598/IisICKnirrICELathMQC', 'event_callback': callback }); return false; } </script>
</head>