<!--====== HEADER START ======-->
<header class="header-absolute header-two sticky-header">
    <div class="container container-custom-one">
        <div class="nav-container d-flex align-items-center justify-content-between">
            @include('layout.partials.nav.villas-nav')
        </div>
    </div>
</header>
<!--====== HEADER END ======-->