<!--====== FOOTER PART START ======-->
	<footer>
		<div class="pt-60 pb-60" class="footer-villas">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="subscribe-text text-center">
							<div class="footer-logo mb-45">
								<img src="{{ asset ('assets/img/villas/logo-villas-light.svg') }}" alt="images">
							</div>
							<p class="txt-under"><i class="fal fa-map-marker-alt"></i>Avenida Coba Norte s/n, Coba Norte, 77780 Tulum, Q.R.</p>
							<p class="txt-under"><i class="fal fa-phone"></i><a href="tel:+529842762391" target="_blank" rel="noopener noreferrer nofollow">+52 (984) 276 23 91</a></p>
							<p class="txt-under"><i class="fal fa-envelope"></i><a href="mailto:sales@lotusrivieramaya.com" target="_blank" rel="noopener noreferrer nofollow">sales@lotusrivieramaya.com</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  -->
		<div class="copyright-area pt-10 pb-10">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 text-center">
						<p class="copyright-text">VILLAS ACCRA TODOS LOS DERECHOS RESERVADOS ® | DEVELOPED BY <a href="https://lazaro.agency" target="_blank" rel="noopener noreferrer">LÁZARO</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
    <!--====== FOOTER PART END ======-->
	<!-- <div id="fb-root"></div>
      	<script>
      	  window.fbAsyncInit = function() {

      	    FB.init({

      	      xfbml            : true,

      	      version          : 'v10.0'

      	    });

      	  };
      	  (function(d, s, id) {

      	    var js, fjs = d.getElementsByTagName(s)[0];

      	    if (d.getElementById(id)) return;

      	    js = d.createElement(s); js.id = id;

      	    js.src = 'https://connect.facebook.net/es_LA/sdk/xfbml.customerchat.js';

      	    fjs.parentNode.insertBefore(js, fjs);

      	  }(document, 'script', 'facebook-jssdk')); -->

      	</script>
      	<!-- Your plugin de chat code -->
      	<!-- <div class="fb-customerchat"

        attribution="page_inbox"

        page_id="112146047306288">
      	</div> -->