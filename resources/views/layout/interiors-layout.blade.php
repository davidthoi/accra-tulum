<!DOCTYPE html>
<html lang="en">

@include('layout.partials.head')

<body>
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
@include('layout.partials.preloader')
@include('layout.partials.header-interiors')

@yield('content')
    
@include('layout.partials.back-to-top')
@include('layout.partials.footer')
@include('layout.partials.scripts')
@stack('scripts')
</body>

</html>