@extends('layout.main-layout')
@section('title', 'Inicio')
@section('content')
    <!--====== BANNER PART START ======-->
    <section class="banner-area banner-style-two" id="bannerSlider">
        <div class="single-banner d-flex align-items-center justify-content-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="banner-content text-center">
                            <img class="title" data-animation="fadeInLeft" data-delay=".9s" src="assets/img/inicio/accra_logo1.svg" alt="logo" style="margin-top:50px;">
                            <span class="promo-tag" data-animation="fadeInDown" data-delay=".6s" style="padding: 0 10% 30px 10% !important;">
                            SOFISTICADO, MODERNO Y AMBIENTALMENTE SUSTENTABLE; SOMOS UN PROYECTO INMOBILIARIO
                            DE VANGUARDIA INTERNACIONAL CON INFRAESTRUCTURA EXCEPCIONAL
                            </span>
                            <!--<ul>
                                <li data-animation="fadeInUp" data-delay="1.3s">
                                    <a class="main-btn btn-border" href="https://wa.link/ieawpj" target="_blank">Contáctanos <i class="fab fa-whatsapp"></i></a>
                                </li>
                            </ul>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- banner bg -->
            <div class="banner-bg" style="background-image: url(assets/img/inicio/slider-principal2.jpg); animation: zoomIn 20s linear;"></div>
            <div class="banner-overly"></div>
        </div>
        <!--<div class="single-banner d-flex align-items-center justify-content-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="banner-content text-center">
                            <img class="title" data-animation="fadeInLeft" data-delay=".9s" src="assets/img/inicio/accra_logo1.svg" alt="logo">
                            <span class="promo-tag" data-animation="fadeInDown" data-delay=".6s">SOFISTICADO, MODERNO Y AMBIENTALMENTE <br> SUSTENTABLE; SOMOS UN PROYEXTO INMOBILIARIO DE <br> VANGUARDIA INTERNACIONAL CON INFRAESTRUCTURA <br> EXCEPCIONAL</span>
                            <ul>
                                <li data-animation="fadeInUp" data-delay="1.3s">
                                    <a class="main-btn btn-border" href="#">Contáctanos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- banner bg -->
            <!--<div class="banner-bg" style="background-image: url(assets/img/inicio/slider-principal.jpg);"></div>
            <div class="banner-overly"></div>
        </div>-->
    </section>
    <!--====== BANNER PART ENDS ======-->
    <!--====== TEXT BLOCK START ======-->
    <section class="text-block bg-white with-pattern pt-115 pb-115">
        <div class="container">
            <div class="row align-items-center justify-content-center" style="margin:0px !important;">
                <div class="col-lg-6 col-md-10 order-1 order-lg-1">
                    <div class="block-text">
                        <div class="section-title mb-20">
                            <span class="title-tag whitec-resp">DISFRUTA DE UN NUEVO ESTILO DE VIDA</span>
                            <h2 class="whitec-resp">Residencia <br> Exclusiva</h2>
                        </div>
                        <p class="pr-50">
                            Accra, es el primer fraccionamiento que ofrece una colección de lotes residenciales capaces de satisfacer las necesidades de toda la familia, vincularlos con la naturaleza y permitirles crecer en un ambiente de seguridad, calidad y exclusividad. Nuestra oferta residencial rinde homenaje a la antigua cultura maya de Tulum y sus tradiciones.
                        </p>
                        <a href="{{ asset ('brochures/inicio/presentacion-general-accra(2).pdf') }}" target="_blank" class="main-btn btn-filled mt-40">Descargar Brochure</a>
                    </div>
                </div>
                <div class="mt50 col-lg-6 col-md-10 order-2 order-lg-2 wow fadeInRight" data-wow-delay=".3s">
                    <img src="assets/img/inicio/accra-residencial-tulum-plano.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="pattern-wrap">
            <div class="pattern"></div>
        </div>
    </section>
    <!--====== TEXT BLOCK END ======-->
    <!--====== ROOM Gallery CTA START ======-->
	<section class="room-gallery-cta" style="background-image: url(assets/img/inicio/accra-paraiso-en-tulum.jpg);">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="cta-text text-center">
						<span style="color:#fff;">Belleza y modernidad</span>
						<h2>Paraíso en Tulum</h2>
                        <span class="mt-4" style="color:#fff;">Creamos una conexión interior-exterior, en donde la naturaleza y la arquitectura brindan el mejor escenario para disfrutar de un estilo de vida por excelencia, enfocado al aire libre, celebrando a Tulum.</span>
						<ul class="mt-30">
							<li class="wow fadeInUp" data-wow-delay=".3s">
                                <a class="main-btn btn-filled" href="<?php echo url('zona-tulum') ?>">CONOCE LA ZONA</a>
                            </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!--====== ROOM Gallery CTA END ======-->
    <!--====== ROOM SLIDER START ======-->
    <section class="room-slider bg-white pb-100 pb0 pt-115">
        <div class="container-fluid p-0">
            <div class="section-title mb0 mb-80 text-center">
                <span class="title-tag" style="color:#808083;">Materializar tus sueños es nuestro trabajo</span>
                <h2>Vida en Tulum</h2>
                <span style="color:#575757;">Te invitamos a que juntos descubramos algo nuevo, a deleitarte con el <br> despliegue de sorpresas que esta maravillosa ciudad ofrece y, sobre todo, a forjar <br> recuerdos significativos al lado de tu familia y amigos.</span>
            </div>
            <div class="row rooms-slider-two justify-content-center" style="margin: 0px !important;">
                <div class="col-lg-6">
                    <div class="single-rooms-box">
                        <div class="room-img">
                            <div class="img" style="background-image: url(assets/img/inicio/vida-tulum/accra_cenotes.jpg);"></div>
                        </div>
                        <div class="room-desc">
                            <div class="row align-items-center">
                                <div class="col-sm-12">
                                    <h3 class="d-flex justify-content-center"><a href="#" class="txt-under">Cenotes</a></h3>
                                    <p class="d-flex justify-content-center txt-under">El complejo de Accra hace posible que tenga a su alrededor tres <br> espectaculares cenotes: Calavera, Tankah y Zacil-Ha. <br> ¡Adéntrate, nada y bucea en estos antiguos sitios sagrados mayas!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-rooms-box">
                        <div class="room-img">
                            <div class="img" style="background-image: url(assets/img/inicio/vida-tulum/accra_zonadeplaya.jpg);"></div>
                        </div>
                        <div class="room-desc">
                            <div class="row align-items-center">
                                <div class="col-sm-12">
                                    <h3 class="d-flex justify-content-center"><a href="#" class="txt-under">Zona de Playa</a></h3>
                                    <p class="d-flex justify-content-center txt-under">Descubra las mejores playas de Cancún, un paraíso rodeado de aguas turquesas y arena blanca.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-rooms-box">
                        <div class="room-img">
                            <div class="img" style="background-image: url(assets/img/inicio/vida-tulum/accra_centrotulum.jpg);"></div>
                        </div>
                        <div class="room-desc">
                            <div class="row align-items-center">
                                <div class="col-sm-12">
                                    <h3 class="d-flex justify-content-center"><a href="#" class="txt-under">Centro De Tulum</a></h3>
                                    <p class="d-flex justify-content-center txt-under">Recorre a tu manera el centro de esta zona caribeña única <br> de la Riviera Maya, descubre sus mejores rincones y disfruta de un pueblo <br> con todo tipo de facilidades y alrededores de ensueño</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-rooms-box">
                        <div class="room-img">
                            <div class="img" style="background-image: url(assets/img/inicio/vida-tulum/accra_ruinas_tulum.jpg);"></div>
                        </div>
                        <div class="room-desc">
                            <div class="row align-items-center">
                                <div class="col-sm-12">
                                    <h3 class="d-flex justify-content-center"><a href="#" class="txt-under">Zona Arqueológica</a></h3>
                                    <p class="d-flex justify-content-center txt-under">A 10 minutos de las residencias de Accra se encuentra el sitio arqueológico <br> mas emblemático de la costa de Quintana Roo. En él se pueden apreciar <br> estructuras mayas, pinturas murales y la muralla que le dio nombre a Tulum.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-rooms-box">
                        <div class="room-img">
                            <div class="img" style="background-image: url(assets/img/inicio/vida-tulum/accra_playadelcarmen.jpg);"></div>
                        </div>
                        <div class="room-desc">
                            <div class="row align-items-center">
                                <div class="col-sm-12">
                                    <h3 class="d-flex justify-content-center"><a href="#" class="txt-under">Playa del Carmen</a></h3>
                                    <p class="d-flex justify-content-center txt-under">Las aguas del Mar Caribe te dan la bienvenida en una ciudad legendaria, <br> antiguamente llamada Xaman Há por la cultura maya, y que hoy en día ofrece <br> un sin fin de aventuras; desde mar y aviarios hasta discotecas y tirolesas.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== ROOM SLIDER END ======-->
    <!--====== CORE FEATURES START ======-->
    <section class="show-amenities core-feature-section bg-white pt-115 pb-115" id="amenities">
        <div class="container">
            <div class="section-title text-lg-left text-center mb-20">
				<span class="title-tag">ZONA EXCLUSIVA DE AMENIDADES</span>
				<h2>Amenities</h2>
			</div>
            <!-- Featre Loop -->
            <div class="row features-loop">
                <div class="col-lg-4 col-sm-6 order-1">
                    <div class="feature-box with-hover-img wow fadeInLeft" data-wow-delay=".3s">
                        <div class="icon">
                            <i class="icon-pool"></i>
                        </div>
                        <h3><a href="#">Alberca</a></h3>
                        <p style="font-size:1.024rem">
                            Centrados en ofrecer todo lo que necesitas para un estilo de vida de lujo y en armonía, Accra Residencial dispone de espaciosas áreas comunes a unos pasos de ti.
                        </p>
                        <span class="count">01</span>
                        <div class="hover-img" style="background-image: url(assets/img/inicio/amenities/accra_alberca-2.jpg);"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-2">
                    <div class="feature-box with-hover-img wow fadeInLeft" data-wow-delay=".5s">    
                        <div class="icon">
                            <i class="icon-dog"></i>
                        </div>
                        <h3>Dogpark</h3>
                        <p>
                            En Accra Residencial es importante para nosotros la diversión y bienestar de todos los miembros de tu familia, nuestro Dogpark es un área desarrollada para la sana recreación en medio de la naturaleza.
                        </p>
                        <span class="count">02</span>
                        <div class="hover-img" style="background-image: url(assets/img/inicio/amenities/accra_dogpark.jpg);"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-3 order-sm-4 order-lg-3">
                    <div class="feature-box with-hover-img wow fadeInRight" data-wow-delay=".7s">
                        <div class="icon">
                            <i class="icon-yoga"></i>
                        </div>
                        <h3><a href="#">Yoga</a></h3>
                        <p>
                            La conexión con la naturaleza es perfecta para la práctica de esta disciplina ancestral, por ello en Accra Residencial ponemos a tu disposición un área dedicada a cuidar de bienestar mental y espiritual.
                        </p>
                        <span class="count">03</span>
                        <div class="hover-img" style="background-image: url(assets/img/inicio/amenities/accra_yoga.jpg);"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-4 order-sm-3 order-lg-4">
                    <div class="feature-box with-hover-img wow fadeInRight" data-wow-delay=".9s">
                        <div class="icon">
                            <i class="icon-sun-bed"></i>
                        </div>
                        <h3><a href="#">Camastros</a></h3>
                        <p>
                            Un día en Tulum debe disfrutarse de forma tranquila, en Accra Residencial contamos con los mejores espacios para tu descanso y relajación, pensados para una experiencia cerca de la naturaleza.
                        </p>
                        <span class="count">04</span>
                        <div class="hover-img" style="background-image: url(assets/img/inicio/amenities/accra_camastros.jpg);"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-5">
                    <div class="feature-box with-hover-img wow fadeInUp" data-wow-delay="1.1s">
                        <div class="icon">
                            <i class="icon-grill"></i>
                        </div>
                        <h3><a href="#">Grill</a></h3>
                        <p>
                            Un espacio para degustar y compartir, Accra Residencial reúne en un solo lugar todo lo que necesitas para disfrutar de la naturaleza en buena compañía y una exquisita comida.
                        </p>
                        <span class="count">05</span>
                        <div class="hover-img" style="background-image: url(assets/img/inicio/amenities/accra_grill.jpg);"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-6">
                    <div class="feature-box with-hover-img wow fadeInUp" data-wow-delay="1.3s">
                        <div class="icon">
                            <i class="icon-table"></i>
                        </div>
                        <h3><a href="#">Picnic</a></h3>
                        <p style="font-size: 13.6px;">
                            Disfruta de la experiencia de vivir en Tulum y compartir momentos maravillosos en compañía de tu familia y amigos, Accra Residencial te ofrece amplias zonas dedicadas a la convivencia y diversión.
                        </p>
                        <span class="count">06</span>
                        <div class="hover-img" style="background-image: url(assets/img/inicio/amenities/accra_alberca.jpg);"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== CORE FEATURES END ======-->
    <!--====== CORE FEATURES RESPONSIVE START ======-->
    <section class="hide-amenities core-feature-section bg-white pt-100 pb-100">
        <div class="container">
            <div class="section-title text-center mb-50">
                <span class="title-tag"> ZONA EXCLUSIVA DE AMENIDADES </span>
                <h2>Amenities</h2>
            </div>
            <!-- Featre Loop -->
            <div class="row features-loop">
                <div class="col-lg-4 col-sm-6 order-1">
                    <div class="feature-box wow fadeInLeft" data-wow-delay=".3s">
                        <div class="icon" style="font-size: 50px;">
                            <i class="icon-pool"></i>
                        </div>
                        <h3><a href="#">Alberca</a></h3>
                        <p>
                        Centrados en ofrecer todo lo que necesitas para un estilo de vida de lujo y en armonía, Accra Residencial dispone de espaciosas áreas comunes a unos pasos de ti.
                        </p>
                        <span class="count">01</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-2">
                    <div class="feature-box wow fadeInDown" data-wow-delay=".4s">
                        <div class="icon">
                            <i class="icon-dog"></i>
                        </div>
                        <h3><a href="#">Dogpark</a></h3>
                        <p>
                        En Accra Residencial es importante para nosotros la diversión y bienestar de todos los miembros de tu familia, nuestro Dogpark es un área desarrollada para la sana recreación en medio de la naturaleza.
                        </p>
                        <span class="count">02</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-3 order-sm-4 order-lg-3">
                    <div class="feature-box wow fadeInRight" data-wow-delay=".5s">
                        <div class="icon" style="font-size: 50px;">
                            <i class="icon-yoga"></i>
                        </div>
                        <h3><a href="#">Yoga</a></h3>
                        <p>
                        La conexión con la naturaleza es perfecta para la práctica de esta disciplina ancestral, por ello en Accra Residencial ponemos a tu disposición un área dedicada a cuidar de bienestar mental y espiritual.
                        </p>
                        <span class="count">03</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-4 order-sm-3 order-lg-4">
                    <div class="feature-box wow fadeInLeft" data-wow-delay=".6s">
                        <div class="icon">
                            <i class="icon-sun-bed"></i>
                        </div>
                        <h3><a href="#">Camastros</a></h3>
                        <p>
                        Un día en Tulum debe disfrutarse de forma tranquila, en Accra Residencial contamos con los mejores espacios para tu descanso y relajación, pensados para una experiencia cerca de la naturaleza.
                        </p>
                        <span class="count">04</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-5">
                    <div class="feature-box wow fadeInUp" data-wow-delay=".7s">
                        <div class="icon">
                            <i class="icon-grill"></i>
                        </div>
                        <h3><a href="#">Grill</a></h3>
                        <p>
                        Un espacio para degustar y compartir, Accra Residencial reúne en un solo lugar todo lo que necesitas para disfrutar de la naturaleza en buena compañía y una exquisita comida.
                        </p>
                        <span class="count">05</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-6">
                    <div class="feature-box wow fadeInRight" data-wow-delay=".8s">
                        <div class="icon" style="font-size: 50px;">
                            <i class="icon-table"></i>
                        </div>
                        <h3><a href="#">Picnic</a></h3>
                        <p>
                        Disfruta de la experiencia de vivir en Tulum y compartir momentos maravillosos en compañía de tu familia y amigos, Accra Residencial te ofrece amplias zonas dedicadas a la convivencia y diversión.
                        </p>
                        <span class="count">06</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== CORE FEATURES RESPONSIVE END ======-->
    <!--====== VIDEO WRAP START ======-->
    <!--<section class="video-wrap full-section" style="background-image: url(assets/img/inicio/video.jpg);">
        <a href="https://www.youtube.com/watch?v=EEJFMdfraVY" class="popup-video wow fadeInDown" data-wow-delay=".3s">
            <img src="assets/img/icon/07.png" alt="Icon">
        </a>
    </section>-->
    <!--====== VIDEO WRAP END ======-->
    <!--====== TEXT BLOCK START ======-->
    <section class="text-block pb-100 bgc-white">
        <div class="container">
            <div class="row align-items-center justify-content-center wow fadeInLeft" data-wow-delay=".3s">
                <div class="col-lg-6 order-2 order-lg-1 text-center">
                    <div class="gallery-single">
                        <div>
                            <a href="assets/img/inicio/sustainable/accra-conjunto.jpg" class="gallery-popup">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-conjunto-thumb.jpg" class="d-md-none d-lg-block" alt="Conjunto Accra Tulum">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-conjunto-thumb-md.jpg" class="d-none d-md-block d-lg-none" alt="Conjunto Accra Tulum">
                            </a>
                        </div>
                        <div>
                            <a href="assets/img/inicio/sustainable/accra-asador.jpg" class="gallery-popup">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-asador-thumb.jpg" class="d-md-none d-lg-block" alt="Asadores Accra Tulum">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-asador-thumb-md.jpg" class="d-none d-md-block d-lg-none" alt="Asadores Accra Tulum">
                            </a>
                        </div>
                        <div>
                            <a href="assets/img/inicio/sustainable/accra-lote.jpg" class="gallery-popup">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-lote-thumb.jpg" class="d-md-none d-lg-block" alt="Accra Tulum Lotes">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-lote-thumb-md.jpg" class="d-none d-md-block d-lg-none" alt="Accra Tulum Lotes">
                            </a>
                        </div>
                        <div>
                            <a href="assets/img/inicio/sustainable/accra-patio-interno.jpg" class="gallery-popup">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-patio-interno-thumb.jpg" class="d-md-none d-lg-block" alt="Patio Interno Accra Tulum">
                                <img src="assets/img/inicio/sustainable/thumbs/accra-patio-interno-thumb-md.jpg" class="d-none d-md-block d-lg-none" alt="Patio Interno Accra Tulum">
                            </a>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center mt-4">
                        <div class="latest-post-arrow arrow-style text-right"></div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-10 order-1 order-lg-2 text-center text-lg-left mb-5 mb-lg-0 wow fadeInRight" data-wow-delay=".5s">
                    <div class="block-text">
                        <div class="section-title mb-20">
                            <span class="title-tag" style="color:#808083;">Entornos socialemente responsables</span>
                            <h2>Sustentabilidad</h2>
                        </div>
                        <p>Accra te ofrece un nuevo estándar para el diseño residencial con visión de futuro, protegiendo y respetando el entorno, siendo socialmente responsables. Cada inmueble cobra vida bajo estrictas prácticas arquitectónicas; incluida la utilización de materiales de construcción ecológicos, conservando y maximizando la vegetación, un diseño eficiente a traves de un arquitectura inteligente que minimiza el consumo de energía y el impacto al medio ambiente.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== TEXT BLOCK END ======-->
    <!--====== TEXT BLOCK START ======-->
    <section id="comercial" class="banner-area banner-style-one text-block pt-115 pb-50 bgc-white">
        <div class="container">
            <div class="row align-items-center justify-content-center wow fadeInLeft" data-wow-delay=".3s">
                <div class="col-lg-6 text-center text-lg-left mb-5 mb-lg-0 wow fadeInRight" data-wow-delay=".5s">
                    <div class="block-text">
                        <div class="section-title mb-20">
                            <span class="title-tag" style="color:#808083;">Hacemos tu día a día más cómodo</span>
                            <h2>Plaza Comercial</h2>
                        </div>
                        <p class="color:#575757;">Contamos con un área de plaza comercial situada en la entrada del Accra Residencial, que incluye tiendas de autoservicio y de primera necesidad; Nos aseguramos de brindarle una variedad de opciones, productos y servicios para hacer de su día a día lo mas cómodo posible.</p>
                        <a href="<?php echo url('plaza-comercial') ?>" class="main-btn btn-filled mt-40">Explorar</a>
                    </div>
                </div>
                <div class="col-lg-6 order-2 wow fadeInRight" data-wow-delay="0.5s">
					<div class="banner-thumb">
						<div class="hero-slider-one">
							<div class="single-thumb">
								<img src="assets/img/inicio/plaza_comercial_accra.jpg" alt="images">
							</div>
							<div class="single-thumb">
                            <img src="assets/img/inicio/accra-comercial-fachada-nocturna.jpg" alt="images">
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </section>
    <!--====== TEXT BLOCK END ======-->
    <!--====== CONTACT PART START ======-->
    <section class="contact-part pt-115 bgc-white">
        <div class="container">
            <!-- Contact Form -->  
            <div id="contacto">
                <div class="section-title text-center">
                    <span class="title-tag">LA PRIMERA COMUNIDAD PLANEADA DE TULÚM</span>
                    <h2>Contáctanos</h2>
                    <span>Envíanos un mensaje con tus dudas y/o comentarios y pronto un asesor se pondrá en contacto contigo, gracias!</span>
                </div>
                <form role="form" class="contact-form">
                @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-30">
                                <span class="icon"><i class="far fa-user" style="color:#4E7D89;"></i></span>
                                <input type="text" placeholder="Su nombre" name="name" id="name" required> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-30">
                                <span class="icon"><i class="far fa-phone" style="color:#4E7D89;"></i></span>
                                <input type="text" placeholder="Su número telefónico" name="phone" id="phone" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-30">
                                <span class="icon"><i class="far fa-envelope" style="color:#4E7D89;"></i></span>
                                <input type="email" placeholder="Su correo electrónico" name="email" id="email" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group textarea mb-30">
                                <span class="icon"><i class="far fa-pen" style="color:#4E7D89;"></i></span>
                                <textarea type="text" placeholder="Escriba su mensaje" name="message" id="message" required></textarea>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="main-btn btn-filled">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End of Contact Form -->
            <!-- Contact Maps -->
        </div>
        <div class="container-fluid p-0">
            <div style="display:flex; justify-content:center;">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11525.704311130043!2d-87.46062107163021!3d20.21944871964811!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjDCsDEzJzMwLjMiTiA4N8KwMjcnMjYuNiJX!5e0!3m2!1ses!2smx!4v1605657605173!5m2!1ses!2smx" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </section>
    <!--====== INSTAGRAM FEED PART START ======-->
    <section class="instagram-feed-section">
        <div class="container-fluid p-0">
            <div class="instagram-slider">
                <div class="image">
                    <a href="assets/img/inicio/instagram/accra-calle-interior-01.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/thumbs/accra-calle-interior-01-thumb.jpg" alt="Accra Tulum Calle Interior">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/galeria_accra2.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/galeria_accra2_thumb.jpg" alt="instagram-feed">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/thumbs/accra-conjunto-01.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/thumbs/accra-conjunto-01-thumb.jpg" alt="Accra Tulum Conjunto">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/accra-gym.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/thumbs/accra-gym-thumb.jpg" alt="instagram-feed">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/galeria_accra5.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/galeria_accra5_thumb.jpg" alt="instagram-feed">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/accra-plaza-alberca.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/thumbs/accra-plaza-alberca-thumb.jpg" alt="instagram-feed">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/accra-we-work.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/thumbs/accra-we-work-thumb.jpg" alt="instagram-feed">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/accra-calle-interior-02.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/thumbs/accra-calle-interior-02-thumb.jpg" alt="instagram-feed">
                    </a>
                </div>
                <div class="image">
                    <a href="assets/img/inicio/instagram/accra-conjunto-02.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/thumbs/accra-conjunto-02-thumb.jpg" alt="instagram-feed">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--====== INSTAGRAM FEED PART END ======-->
    
    <!--====== ROOM SLIDER START ======-->
	<!-- <section class="room-slider">
		<div class="container-fluid p-0">
			<div class="row rooms-slider-one">
				<div class="col">
                    <a href="assets/img/inicio/instagram/galeria_accra6.jpg" class="insta-popup">
                        <img src="assets/img/inicio/instagram/galeria_accra6_thumb.jpg" alt="instagram-feed">
                    </a>
				</div>
				<div class="col">
					<div class="slider-img" style="background-image: url(assets/img/room-slider/02.jpg);"></div>
				</div>
				<div class="col">
					<div class="slider-img" style="background-image: url(assets/img/room-slider/03.jpg);"></div>
				</div>
				<div class="col">
					<div class="slider-img" style="background-image: url(assets/img/room-slider/04.jpg);"></div>
				</div>
				<div class="col">
					<div class="slider-img" style="background-image: url(assets/img/room-slider/05.jpg);"></div>
				</div>
			</div>
		</div>
		<div class="rooms-content-wrap">
			<div class="container">
				<div class="row justify-content-center justify-content-md-start">
					<div class="col-xl-4 col-lg-5 col-sm-8">
						<div class="room-content-box" style="display:none;">
							<div class="slider-count"></div>
							<div class="slider-count-big"></div>
							<div class="room-content-slider">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!--====== ROOM SLIDER END ======-->

@endsection
@push('scripts')
<script src="{{ asset('assets/js/mail.contacto.js') }}"></script>
<script src="{{ asset('assets/js/notify.js') }}"></script>
@endpush