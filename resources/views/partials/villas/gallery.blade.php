<div class="col-lg-6">
    <div class="single-rooms-box">
        <div class="room-img">
            <div class="img" style="background-image: url({{url('assets/img/villas/gallery/01-sala-de-estar.jpg')}});">
            </div>
        </div>
        <div class="room-desc">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h3>Sala de Estar</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="single-rooms-box">
        <div class="room-img">
            <div class="img" style="background-image: url({{url('assets/img/villas/gallery/02-roof-top.jpg')}});">
            </div>
        </div>
        <div class="room-desc">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h3>Rooftop</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="single-rooms-box">
        <div class="room-img">
            <div class="img" style="background-image: url({{url('assets/img/villas/gallery/03-roof-garden.jpg')}});">
            </div>
        </div>
        <div class="room-desc">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h3>Roof Garden</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="single-rooms-box">
        <div class="room-img">
            <div class="img" style="background-image: url({{url('assets/img/villas/gallery/04-master-bedroom.jpg')}});">
            </div>
        </div>
        <div class="room-desc">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h3>Master Bedroom</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="single-rooms-box">
        <div class="room-img">
            <div class="img" style="background-image: url({{url('assets/img/villas/gallery/05-fachada-principal.jpg')}});">
            </div>
        </div>
        <div class="room-desc">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h3>Fachada Principal</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="single-rooms-box">
        <div class="room-img">
            <div class="img" style="background-image: url({{url('assets/img/villas/gallery/06-master-bathroom.jpg')}});">
            </div>
        </div>
        <div class="room-desc">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h3>Master Bathroom</h3>
                </div>
            </div>
        </div>
    </div>
</div>