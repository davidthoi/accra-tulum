<section class="banner-area banner-style-two" id="bannerSlider">
    <div class="single-banner d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="banner-content text-center mt-5">
                        <h1 data-animation="fadeInDown" data-delay="1.5s" class="mb-3">Bienvenido a tu nuevo hogar en Tulum</h1>
                        <span data-animation="fadeInUp" data-delay="1.7s" class="mb-5">Vive rodeado de lujo y naturaleza dentro de un concepto moderno, único en la Riviera Maya. </span>
                        <ul>
                            <li data-animation="fadeIn" data-delay="2s">
                                <a class="villa-btn" href="#" data-toggle="modal" data-target="#villasForm">Contáctanos</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner bg -->
        <div class="banner-bg" style="background-image: url({{url('assets/img/villas/slider-1.jpg')}}); animation: zoomIn 20s linear;"></div>
        <div class="banner-overly"></div>
    </div>
</section>