<div class="row d-flex">
    <div class="col-lg-5 order-1 order-lg-2 text-center text-md-left pl-md-4 align-self-md-center mb-4 mg-md-5 mb-lg-0">
        <h3>“Tzalam”, Kumaru, Chechen</h3>
        <div class="d-inline-flex justify-content-center justify-content-md-start "><div class="line"></div></div>
        <p class="materials-text">Madera tropical resistente al clima y al paso del tiempo. Dan a cada espacio el toque que te conectan con la naturaleza.</p>
    </div>
    <div class="col-lg-7 order-1 order-lg-1"><img src="{{asset ('assets/img/villas/materiales/madera.jpg')}}" alt="Madera Tropical" srcset=""> </div>
</div>
<div class="row d-flex">
    <div class="col-lg-5 order-1 order-lg-2 text-center text-md-left pl-md-4 align-self-md-center mb-4 mg-md-5 mb-lg-0">
        <h3>Block local para mampostería</h3>
        <div class="d-inline-flex justify-content-center justify-content-md-start "><div class="line"></div></div>
        <p class="materials-text">Además de una buena apariencia, la producción de cada block está alineado a criterios sostenibles que reducen el impacto ambiental.</p>
    </div>
    <div class="col-lg-7 order-1 order-lg-1"><img src="{{asset ('assets/img/villas/materiales/block.jpg')}}" alt="Block Mamposteria" srcset=""> </div>
</div>
<div class="row d-flex">
    <div class="col-lg-5 order-1 order-lg-2 text-center text-md-left pl-md-4 align-self-md-center mb-4 mg-md-5 mb-lg-0">
        <h3>Pasta para aplanado “Chago”, tipo Chukum, rojiza</h3>
        <div class="d-inline-flex justify-content-center justify-content-md-start "><div class="line"></div></div>
        <p class="materials-text">Material exclusivo de la región, utilizado para mantener frescas todas las habitaciones con un estilo único y natural.</p>
    </div>
    <div class="col-lg-7 order-1 order-lg-1"><img src="{{asset ('assets/img/villas/materiales/pasta.jpg')}}" alt="Pasta para aplanado" srcset=""> </div>
</div>
<div class="row d-flex">
    <div class="col-lg-5 order-1 order-lg-2 text-center text-md-left pl-md-4 align-self-md-center mb-4 mg-md-5 mb-lg-0">
        <h3>Cristal</h3>
        <div class="d-inline-flex justify-content-center justify-content-md-start "><div class="line"></div></div>
        <p class="materials-text">Permite que cada residencia se ilumine naturalmente, aprovechando al máximo la luz solar.</p>
    </div>
    <div class="col-lg-7 order-1 order-lg-1"><img src="{{asset ('assets/img/villas/materiales/cristal.jpg')}}" alt="Madera Tropical" srcset=""> </div>
</div>