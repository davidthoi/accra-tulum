<!-- Modal -->
<div class="modal fade" id="villasForm" tabindex="-1" role="dialog" aria-labelledby="villasForm" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row mb-4">
                        <div class="col-12 text-center">
                            <h3 class="modal-title">ENVÍANOS UN MENSAJE</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form role="form" class="modal-form">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-group mb-30">
                                            <span class="icon"><i class="far fa-user"></i></span>
                                            <input type="text" placeholder="Nombre" name="name" id="name" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group mb-30">
                                            <span class="icon"><i class="far fa-envelope"></i></span>
                                            <input type="email" placeholder="Correo electrónico" name="email" id="email" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group mb-30">
                                            <span class="icon"><i class="far fa-phone"></i></span>
                                            <input type="text" placeholder="Teléfono" name="phone" id="phone" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group textarea mb-30">
                                            <span class="icon"><i class="far fa-pen"></i></span>
                                            <textarea type="text" placeholder="Mensaje" name="message" id="message" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center pb-5">
                                        <button type="submit" class="villa-btn">ENVIAR MENSAJE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ asset('assets/js/modal.contacto.js') }}"></script>
<script src="{{ asset('assets/js/notify.js') }}"></script>
@endpush