@extends('layout.villas-layout')
@section('title', 'Villas')
@push('styles')
<link rel="stylesheet" href="{{ URL::asset('assets/css/villas.css') }}" />
@endpush
@section('content')
    <!--====== BANNER PART START ======-->
    @include('partials.villas.banner')
    <!--====== BANNER PART ENDS ======-->
    <!--====== ABOUT VILLAS START ======-->
    <section class="bg-texture">
        <div class="container">
            <div class="palm-1 wow fadeInDown" data-wow-delay=".3s"><img src="{{asset('assets/img/villas/palms/palm-01.png')}}" alt=""></div>
            <div class="palm-2 wow fadeInUp" data-wow-delay=".8s""><img src="{{asset('assets/img/villas/palms/palm-02.png')}}" alt=""></div>
            <div class="row text-center">
                <div class="col-lg-12">
                    <h5 class="wow fadeInDown" data-wow-delay=".4s">Tu nueva residencia en Tulum está  en</h5>
                    <h2 class="mt-3 mb-4 wow fadeInUp" data-wow-delay=".5s"> Villas ACCRA</h2>
                    <div class="d-flex justify-content-center wow fadeInRight" data-wow-delay=".6s"><div class="title-line"></div></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 align-self-center order-1 order-lg-2 pl-lg-4 mb-5 mb-lg-0 text-center text-lg-left">
                    <div class="fadeInUp wow" data-wow-delay=".6s">
                        <p>El diseño de las villas retoma las <strong>características principales de la antigua cultura maya,</strong> en el interior como en el exterior, creando una atmósfera cargada de simbolismo que <strong>se mezcla a la perfección con el diseño contemporáneo,</strong> obteniendo un lugar atemporal moderno, lujoso y natural.</p>
                        <p>Diseñada para proporcionar una estancia acogedora, sus amplios interiores son <strong>ideales para íntimas veladas o grandes reuniones.</strong></p>
                    </div>
                </div>
                <div class="col-lg-7 order-2 order-lg-1 wow fadeInRight" data-wow-delay=".7"><img src="{{asset ('assets/img/villas/fachada-accra-tulum-villas.jpg') }}" class="img-fluid" alt="Fachada de Accra Villas"></div>
            </div>
        </div>
    </section>
    <!--====== SECOND SECTION ======-->
    <section class="text-block with-pattern pt-115 pb-115 bg-raw">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="block-text pr-15 wow fadeInUp" data-wow-delay=".5">
                        <p>La cocina contigua a un luminoso comedor y sala de estar, <strong>genera espacios de convivencia en los que tendrás la sensación de estar en el exterior.</strong></p>
                        <p> Las espaciosas habitaciones ofrecen <strong>privacidad mientras se disfruta de la vista de la selva y la brisa del mar,</strong> todos los días un motivo para enamorarse de su esplendor.</p>
                        <p> La alberca privada del rooftop se convierte en <strong>un lugar místico para relajarse con el mar como testigo.</strong></p>
                        <a href="{{asset('brochures/villas/press-villas-accra.pdf')}}" target="_blank" rel="noopener noreferrer"class="villa-btn light mt-30">DESCARGAR BROCHURE</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-10 mt-5">
                    <div class="video-wrap video-wrap-two wow fadeInRight" data-wow-delay=".7s" style="background-image: url({{url('assets/img/villas/master-bedroom.jpg')}});">
                    </div>
                </div>
            </div>
        </div>
        <div class="pattern-wrap wow fadeIn" data-wow-delay=".5s">
            <div class="pattern"></div>
        </div>
    </section>
    <!--====== ABOUT VILLAS ENDS ======-->
    <!--====== GALERIA ======-->
    <section class="room-slider bg-white pb-10 pt-90">
        <div class="container-fluid p-0">
            <div class="section-title text-center">
                <h2 class="mb-4 wow fadeInDown" data-wow-delay=".5s">GALERÍA</h2>
                <div class="d-flex justify-content-center wow fadeInRight" data-wow-delay=".7s"><div class="title-line"></div></div>
            </div>
            <div class="row rooms-slider-two justify-content-center m-0 wow fadeIn" data-wow-delay=".7">
                @include('partials.villas.gallery')
            </div>
        </div>
    </section>
    <!--====== GALERIA ENDS ======-->
     <!--====== 360 CAROUSEL ======-->
     <section id="tressesenta">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="mb-4 wow fadeInDown" data-wow-delay=".5s">ACCRA EN 360°</h2>
                <div class="d-flex justify-content-center wow fadeInRight" data-wow-delay=".7s"><div class="title-line"></div></div>
            </div>
            <iframe id="frame-360" src="https://momento360.com/e/uc/3d4b19cfc06e45bab686c8e0b130a9c4?utm_campaign=embed&utm_source=other&size=medium" frameborder="0"></iframe>
        </div>
    </section>
    <!--====== 360 CAROUSEL END ======-->
    <!--====== MATERIALES ======-->
    <section id="materials" class="bg-texture">
        <div class="container">
            <div class="row mb-50">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center">
                    <h2 class="mb-4 wow fadeInUp" data-wow-delay=".5s">materiales</h2>
                    <div class="d-flex justify-content-center wow fadeInRight" data-wow-delay=".7s"><div class="title-line"></div></div>
                    <p class="wow fadeInLeft" data-wow-delay=".9s"><strong>Con sus terminados en madera y colores mediterráneos</strong> cada espacio en la villa se vuelve parte de <strong>un ritual para alinear cuerpo, mente y alma con la naturaleza.</strong> Los materiales exclusivos de la región <strong>permiten que la energía de la selva</strong> que rodea a la residencia fluya constantemente a través de la casa,<strong> purificando su interior.</strong></p>
                </div>
            </div>
            <div class="materials-slider wow fadeIn" data-wow-delay=".9s">
                @include('partials.villas.carousel')
            </div>
        </div>
        <div class="container mt-40">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div class="materials-arrow arrow-style text-right"></div>
                </div>
            </div>
		</div>
        <div class="palm-3 wow fadeInUp" data-wow-delay="1.1s"><img src="{{asset('assets/img/villas/palms/palm-03.png')}}" alt=""></div>
        <div class="palm-4 wow fadeInUp" data-wow-delay="1.3s"><img src="{{asset('assets/img/villas/palms/palm-04.png')}}" alt=""></div>
    </section>
    <!--====== MATERIALES ENDS ======-->
    <!--====== BANNER ======-->
    <section class="banner-bg">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2 text-center">
                    <p class="wow fadeInDown" data-wow-delay=".5s">Habitar en Villas Accra es una experiencia única. Contáctanos para recibir información.</p>
                    <div class="d-flex justify-content-center wow fadeInRight" data-wow-delay=".6s"><div class="title-line"></div></div>
                    <a href="#" class="villa-btn light mt-30 wow fadeInUp" data-wow-delay=".7s" data-toggle="modal" data-target="#villasForm">CONTACTAR AHORA</a>
                </div>
            </div>
        </div>
    </section>
    <!--====== BANNER ENDS ======-->
     <!--====== MAPA ======-->
     <section class="pt-80">
        <div class="container-fluid p-0">
            <div class="row m-0">
                <div class="col-12">
                    <div class="section-title text-center">
                        <h2 class="mb-4 wow fadeInDown" data-wow-delay=".5s">ubicación</h2>
                        <div class="d-flex justify-content-center wow fadeInRight" data-wow-delay=".6s"><div class="title-line"></div></div>
                    </div>
                </div>
                <div class="col-12 p-0 wow fadeIn" data-wow-delay=".7s">
                    <div class="contact-maps" id="contactMaps" style="height:600px;"></div>
                </div>
            </div>
        </div>
    </section>
    <!--====== MAPA ENDS ======-->
    @include('partials.villas.modal')
@endsection
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4YQYFQh4aqdhK8A9C5YXjTdZlI-DRs_4"></script>
<script src="{{asset('assets/js/map.js') }}"></script>
@endpush