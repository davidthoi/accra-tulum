@extends('layout.interiors-layout')
@section('title', 'Plaza Comercial')
@section('content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url({{ asset ('assets/img/comercial/plazacomercial_accra.jpg') }}">
        <div class="container">
            <div class="breadcrumb-text">
                <h2 class="page-title">Plaza Comercial</h2>
                <!--<ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">About</li>
                </ul>-->
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <!--====== TEXT BLOCK START ======-->
    <section class="text-block pt-115 pb-115 mbn-60 pt60-pb0" style="background-color:#ECF0F2;">
        <div class="container">
            <div class="row align-items-center justify-content-center wow fadeInLeft" data-wow-delay=".3s" >
                <div class="col-lg-7 orden-2 mt-40">
                    <div class="text-img text-center text-lg-left mb-small" style="max-width:90%;">
						<div class="slider-comercial">
                        	<img src="{{ asset ('assets/img/comercial/plata-alta-comercial.jpg') }}" alt="Image">
							<img src="{{ asset ('assets/img/comercial/planta-baja-comercial.jpg') }}" alt="Image">
						</div>
						<!--<div class="pa-btn">Planta Alta</div>
						<div class="pb-btn">Planta Baja</div>-->
						<div class="contenedor-plantas">
							<div>
								<input type="radio" id="pa-btn" name="plantas" checked style="color:red;">Planta Alta
							</div>
							<div>
								<input type="radio" id="pb-btn" name="plantas">Planta Baja
							</div>
						</div>
					</div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-10 wow fadeInRight" data-wow-delay=".5s">
                    <div class="block-text">
                        <div class="section-title mb-20">
                            <span class="title-tag">Centro comercial cerca de ti</span>
                            <h2>Comercial Accra</h2>
                        </div>
                        <p>
						Accra Residencial cuenta con su propia plaza comercial, ofrece una amplia gama de tiendas de autoservicios, y con los servicios necesarios para ofrecer a sus clientes comodidad y confort como, motor lobby cubierto, elevadores, escaleras eléctricas, rampas, baños familiares, servicio de seguridad, cámaras de vigilancia, en cada extremo del centro comercial.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== TEXT BLOCK END ======-->
    <!--====== CALL TO ACTION END ======-->
	<section class="cta-section pt-115 pb-50">
		<div class="container">
            <div class="section-title text-center mb-50">
				<span class="title-tag"> Dedicados a cuidar tu estilo de vida </span>
                <h2>Especificaciones <br> Técnicas</h2>
            </div>
			<div class="cta-inner">
				<div class="row justify-content-center">
					<!--<div class="col-lg-4 col-md-8 col-sm-9 col-10 order-2 order-lg-1">
						<div class="cta-text">
							<div class="section-title mb-20">
								<span class="title-tag">Dedicados a cuidar tu estilo de vida</span>
								<h2>Especificaciones Técnicas</h2>
							</div>
							<p>Miranda has everything for your trip
								& every single things.</p>
							<a href="#" class="main-btn btn-filled">take a tour</a>
						</div>
					</div>-->
					<div class="col-lg-4 col-md-10 col-sm-11 col-10 order-1 order-lg-2">
						<!-- feature loop -->
						<div class="cta-features">
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".3s">
								<div class="icon bgc-esptec">
									<i class="icon-survilance"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Vigilancia 24/7</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">01</span>
								</div>
							</div>
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-camera"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Camaras de Vigilancia</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">04</span>
								</div>
							</div>
							<!-- feature box -->
						</div>
                    </div>
                    <div class="col-lg-4 col-md-10 col-sm-11 col-10 order-1 order-lg-2">
						<!-- feature loop -->
						<div class="cta-features">
							<!-- feature box -->
							<div class="mt50 single-feature wow fadeInUp" data-wow-delay=".3s">
								<div class="icon bgc-esptec">
									<i class="icon-access"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Control de Acceso</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">02</span>
								</div>
							</div>
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-public-lamp"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Iluminación de Luz</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">05</span>
								</div>
							</div>
                            <!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-battery"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Planta de Emergencia</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">07</span>
								</div>
							</div>
							<!-- feature box -->
						</div>
                    </div>
                    <div class="col-lg-4 col-md-10 col-sm-11 col-10 order-1 order-lg-2">
						<!-- feature loop -->
						<div class="cta-features">
							<!-- feature box -->
							<div class="mt50 single-feature wow fadeInUp" data-wow-delay=".3s">
								<div class="icon bgc-esptec">
									<i class="icon-energy"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Acometida de Luz</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">03</span>
								</div>
							</div>
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-water-pipe"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Acometida de Agua</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">06</span>
								</div>
							</div>
                            <!-- feature box -->
						</div>
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!--====== CALL TO ACTION END ======-->
@endsection
@push('scripts')
<script>
$('.slider-comercial').slick({
  prevArrow:'#pa-btn',
  nextArrow:'#pb-btn',
  arrows: true,
  infinite: false,
  speed: 500,
  fade: true,
  dots: false,
  autoplay: false,
  cssEase: 'linear'
});
</script>
@endpush
