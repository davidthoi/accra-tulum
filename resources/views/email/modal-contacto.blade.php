@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
    @include('beautymail::templates.minty.contentStart', ['color' => '#072651'])
		<tr>
			<td class="title">
                Contacto Villas ACCRA
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td>
                <h4>Nombre: {{ $data['name'] }}</h4>
                <h4>Teléfono: {{ $data['phone'] }}</h4>
                <h4>Correo: {{ $data['email'] }}</h4>
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
		<tr>
			<td class="title">
				Mensaje
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
                {{ $data['message'] }}
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
		<tr>
			<td>
				<!-- @include('beautymail::templates.minty.button', ['text' => 'Sign in', 'link' => '#']) -->
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
	@include('beautymail::templates.minty.contentEnd')

@stop