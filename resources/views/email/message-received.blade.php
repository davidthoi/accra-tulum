<!DOCTYPE html>
<html>
<head>
	<title>Nuevo Mensaje Recibido</title>
</head>
<body>
	<p>Recibiste un mensaje de: {{ $msg['nombre'] }} - {{ $msg['email']}} </p>
	<p>Numero de teléfono: {{ $msg['telefono'] }} </p>
	<p><strong>Contenido:</strong> {{$msg['contenido']}} </p>
</body>
</html>