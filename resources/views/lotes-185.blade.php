@extends('layout.interiors-layout')
@section('title', 'Lotes Residenciales 185m2')
@section('content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url({{ asset ('assets/img/lotes/185/lotes_residenciales_185_accra.jpg') }}">
        <div class="container">
            <div class="breadcrumb-text">
                <h2 class="page-title">Lotes Residenciales</h2>
                <h3 class="h3-subtitle">185m<sup>2</sup></h3>
                <!--<ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">About</li>
                </ul>-->
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <!--====== TEXT BLOCK START ======-->
    <section class="text-block pt-115 pb-115" style="background-color:#ECF0F2;">
        <div class="container">
            <div class="row align-items-center justify-content-center wow fadeInLeft" data-wow-delay=".3s">
                <div class="col-lg-7">
                    <div class="text-img text-center text-lg-left mb-small">
                        <img src="{{ asset ('assets/img/lotes/185/Lotes 185 m2_ActDic2020.jpg') }}" alt="Image">
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-10 wow fadeInRight" data-wow-delay=".5s">
                    <div class="block-text pl-20">
                        <div class="pb-20 side-icon-info"><i class="icon-houses" style="color:#986F51; font-size:2em; padding-right:.5em;"></i>2 LOTES EN TOTAL</div>
                        <div class="side-icon-info"><i class="icon-size" style="color:#986F51; font-size:2em; padding-right:.5em;"></i>9.25m x 20m</div>
                        <!--<div class="mt-40"><a href="https://wa.link/ieawpj" target="_blank" class="btn-asesor">CONTACTAR ASESOR <i class="fab fa-whatsapp"></i></a></div>-->
                        <div class="mt-40"><a href="{{ asset ('brochures/lotes/185/(2)-brochure-lote-185-m2.pdf') }}" target="_blank" class="btn-brochure">DESCARGAR BROCHURE</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== TEXT BLOCK END ======-->
    <!--====== CORE FEATURES START ======-->
    <section class="core-feature-section bg-white pt-115 pb-115">
        <div class="container">
            <div class="section-title text-center mb-50">
                <span class="title-tag"> ZONA EXCLUSIVA DE AMENIDADES </span>
                <h2>Amenities</h2>
            </div>
            <!-- Featre Loop -->
            <div class="row features-loop">
                <div class="col-lg-4 col-sm-6 order-1">
                    <div class="feature-box wow fadeInLeft" data-wow-delay=".3s">
                        <div class="icon" style="font-size: 50px;">
                            <i class="icon-pool"></i>
                        </div>
                        <h3><a href="#">Alberca</a></h3>
                        <p>
                        Centrados en ofrecer todo lo que necesitas para un estilo de vida de lujo y en armonía, Accra Residencial dispone de espaciosas áreas comunes a unos pasos de ti.
                        </p>
                        <span class="count">01</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-2">
                    <div class="feature-box wow fadeInDown" data-wow-delay=".4s">
                        <div class="icon">
                            <i class="icon-dog"></i>
                        </div>
                        <h3><a href="#">Dogpark</a></h3>
                        <p>
                        En Accra Residencial es importante para nosotros la diversión y bienestar de todos los miembros de tu familia, nuestro Dogpark es un área desarrollada para la sana recreación en medio de la naturaleza.
                        </p>
                        <span class="count">02</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-3 order-sm-4 order-lg-3">
                    <div class="feature-box wow fadeInRight" data-wow-delay=".5s">
                        <div class="icon" style="font-size: 50px;">
                            <i class="icon-yoga"></i>
                        </div>
                        <h3><a href="#">Yoga</a></h3>
                        <p>
                        La conexión con la naturaleza es perfecta para la práctica de esta disciplina ancestral, por ello en Accra Residencial ponemos a tu disposición un área dedicada a cuidar de bienestar mental y espiritual.
                        </p>
                        <span class="count">03</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-4 order-sm-3 order-lg-4">
                    <div class="feature-box wow fadeInLeft" data-wow-delay=".6s">
                        <div class="icon">
                            <i class="icon-sun-bed"></i>
                        </div>
                        <h3><a href="#">Camastros</a></h3>
                        <p>
                        Un día en Tulum debe disfrutarse de forma tranquila, en Accra Residencial contamos con los mejores espacios para tu descanso y relajación, pensados para una experiencia cerca de la naturaleza.
                        </p>
                        <span class="count">04</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-5">
                    <div class="feature-box wow fadeInUp" data-wow-delay=".7s">
                        <div class="icon">
                            <i class="icon-grill"></i>
                        </div>
                        <h3><a href="#">Grill</a></h3>
                        <p>
                        Un espacio para degustar y compartir, Accra Residencial reúne en un solo lugar todo lo que necesitas para disfrutar de la naturaleza en buena compañía y una exquisita comida.
                        </p>
                        <span class="count">05</span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 order-6">
                    <div class="feature-box wow fadeInRight" data-wow-delay=".8s">
                        <div class="icon" style="font-size: 50px;">
                            <i class="icon-table"></i>
                        </div>
                        <h3><a href="#">Picnic</a></h3>
                        <p>
                        Disfruta de la experiencia de vivir en Tulum y compartir momentos maravillosos en compañía de tu familia y amigos, Accra Residencial te ofrece amplias zonas dedicadas a la convivencia y diversión.
                        </p>
                        <span class="count">06</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== CORE FEATURES END ======-->
    <!--====== CALL TO ACTION END ======-->
	<section class="cta-section pt-115 pb-50">
		<div class="container">
            <div class="section-title text-center mb-50">
				<span class="title-tag"> Dedicados a cuidar tu estilo de vida </span>
                <h2>Especificaciones <br> Técnicas</h2>
            </div>
			<div class="cta-inner">
				<div class="row justify-content-center">
					<!--<div class="col-lg-4 col-md-8 col-sm-9 col-10 order-2 order-lg-1">
						<div class="cta-text">
							<div class="section-title mb-20">
								<span class="title-tag">Dedicados a cuidar tu estilo de vida</span>
								<h2>Especificaciones Técnicas</h2>
							</div>
							<p>Miranda has everything for your trip
								& every single things.</p>
							<a href="#" class="main-btn btn-filled">take a tour</a>
						</div>
					</div>-->
					<div class="col-lg-4 col-md-10 col-sm-11 col-10 order-1 order-lg-2">
						<!-- feature loop -->
						<div class="cta-features">
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".3s">
								<div class="icon bgc-esptec">
									<i class="icon-survilance"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Vigilancia 24/7</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">01</span>
								</div>
							</div>
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-camera"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Camaras de Vigilancia</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">04</span>
								</div>
							</div>
							<!-- feature box -->
						</div>
                    </div>
                    <div class="col-lg-4 col-md-10 col-sm-11 col-10 order-1 order-lg-2">
						<!-- feature loop -->
						<div class="cta-features">
							<!-- feature box -->
							<div class="mt50 single-feature wow fadeInUp" data-wow-delay=".3s">
								<div class="icon bgc-esptec">
									<i class="icon-access"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Control de Acceso</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">02</span>
								</div>
							</div>
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-public-lamp"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Iluminación de Luz</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">05</span>
								</div>
							</div>
                            <!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-battery"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Planta de Emergencia</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">07</span>
								</div>
							</div>
							<!-- feature box -->
						</div>
                    </div>
                    <div class="col-lg-4 col-md-10 col-sm-11 col-10 order-1 order-lg-2">
						<!-- feature loop -->
						<div class="cta-features">
							<!-- feature box -->
							<div class="mt50 single-feature wow fadeInUp" data-wow-delay=".3s">
								<div class="icon bgc-esptec">
									<i class="icon-energy"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Acometida de Luz</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">03</span>
								</div>
							</div>
							<!-- feature box -->
							<div class="single-feature wow fadeInUp" data-wow-delay=".4s">
								<div class="icon bgc-esptec">
									<i class="icon-water-pipe"></i>
								</div>
								<div class="cta-desc">
									<h3><a href="#">Acometida de Agua</a></h3>
									<!--<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
										nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>-->
									<span class="count">06</span>
								</div>
							</div>
                            <!-- feature box -->
						</div>
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!--====== CALL TO ACTION END ======-->
@endsection
