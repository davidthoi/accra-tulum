@extends('layout.interiors-layout')
@section('title', 'Zona Tulum')
@section('content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url({{ asset ('assets/img/zona/accra_zonatulum.jpg') }}">
        <div class="container">
            <div class="breadcrumb-text">
				<span>CERCA DE LAS MEJORES ZONAS</span>
                <h2 class="page-title">Ubicación Exclusiva</h2>
                <!--<ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">About</li>
                </ul>-->
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <!--====== TEXT BLOCK START ======-->
    <section class="text-block pt-115 pb-115" style="background-color:#ECF0F2;">
        <div class="container">
            <div class="row align-items-center justify-content-center wow fadeInLeft" data-wow-delay=".3s" >
                <div class="col-lg-7">
                    <div class="text-img text-center text-lg-left mb-small" style="max-width:90%;">
                        <img src="{{ asset ('assets/img/zona/mapatulum01.jpg') }}" alt="Image">
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-10 wow fadeInRight" data-wow-delay=".5s">
                    <div class="block-text">
                        <div class="section-title mb-20">
                            <span class="title-tag">Nuestra ubicación</span>
                            <h2>Carretera Tulum Coba</h2>
                        </div>
                        <p>
						A un lado del centro de Tulum y a solo 4 min de la playa y Zona Hotelera.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<!--====== TEXT BLOCK END ======-->
	    <!--====== TEXT BLOCK START ======-->
		<section class="text-block pt-115 pb-115" style="background-color:#fff;">
        <div class="container">
			<div class="section-title text-center mb-50">
                <span class="title-tag"> DISTANCIAS DE</span>
                <h2>Atracciones Cercanas</h2>
            </div>
            <div class="row align-items-center justify-content-center wow fadeInLeft" data-wow-delay=".3s" >
                <div class="col-lg-7">
                    <div class="text-img text-center text-lg-left mb-small">
                        <img src="{{ asset ('assets/img/zona/atracciones-cercanas.jpg') }}" alt="Image">
                    </div>
                </div>
                <div class="col-lg-5 col-md-8 col-sm-10 wow fadeInRight" data-wow-delay=".5s">
                    <div class="block-text">
                        <ul>
							<li class="atracciones-list"><span class="atracciones-numcircle">1</span>Cenote Calavera.<span class="atracciones-time">1 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">2</span>Tulum Centro.<span class="atracciones-time">1 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">3</span>Chedraui.<span class="atracciones-time">1 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">4</span>Cenote Tankah.<span class="atracciones-time">3 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">5</span>Starbucks.<span class="atracciones-time">5 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">6</span>Zona de Playa Tulum.<span class="atracciones-time">4 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">7</span>Cenote Zacil-Ha.<span class="atracciones-time">6 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">8</span>Carretera Mérida.<span class="atracciones-time">2 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">9</span>Carretera Cancún-Tulum.<span class="atracciones-time">5 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">10</span>Carretera Chetumal.<span class="atracciones-time">6 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">11</span>Zona arqueológica Tulum.<span class="atracciones-time">10 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">12</span>Playa del Carmen.<span class="atracciones-time">45 min</span></li>
							<li class="atracciones-list"><span class="atracciones-numcircle">13</span>Aeropuerto Cancún.<span class="atracciones-time">1 hr</span></li>
						</ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== TEXT BLOCK END ======-->

@endsection