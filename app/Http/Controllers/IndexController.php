<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        return view("/");
    }

    public function contacto(Request $request){
        $response = array(
			'status' => false,
            // 'errors' => null,
            // 'statusText' => trans('Hemos recibido su mensaje.')
        );

        try
        {
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('email.mail-contacto', ['data' => $request], function($message)
            {
                $message
                    ->from(env('MAIL_FROM_ADDRESS'))
                    ->to(env('MAIL_TO_ADDRESS'), env('MAIL_FROM_NAME'))
                    ->subject( "Contacto Accra Tulum" );
            });
            $response['status'] = true;
        }
        catch (Throwable $t)
        {
            $response['status'] = false;
        }
        catch(\Exception $e)
        {
            $response['status'] = false;
        }
              
        return $response;
    }

    public function modal(Request $request){
        $response = array(
			'status' => false,
            // 'errors' => null,
            // 'statusText' => trans('Hemos recibido su mensaje.')
        );

        try
        {
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $beautymail->send('email.modal-contacto', ['data' => $request], function($message)
            {
                $message
                    ->from(env('MAIL_FROM_ADDRESS'))
                    ->to(env('MAIL_TO_ADDRESS'), env('MAIL_FROM_NAME'))
                    ->subject( "Contacto Villas Accra Tulum" );
            });
            $response['status'] = true;
        }
        catch (Throwable $t)
        {
            $response['status'] = false;
        }
        catch(\Exception $e)
        {
            $response['status'] = false;
        }
              
        return $response;
    }
}